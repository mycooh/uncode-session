package cn.uncode.session.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.uncode.cache.CacheUtils;
import cn.uncode.cache.framework.Level;

public class DefaultSessionCache implements SessionCache {
	
	private static final String SESSION_STORE_REGION_KEY = "uncode_session_region";
	
	@Override
	public void put(String sessionId, SessionMap sessionMap, int sessionTimeOut) {
		CacheUtils.getCache(SESSION_STORE_REGION_KEY).put(sessionId, sessionMap, sessionTimeOut, Level.Remote);
	}
	
	

	@Override
	public SessionMap get(String sessionId) {
		Object o = CacheUtils.getCache(SESSION_STORE_REGION_KEY).get(sessionId, Level.Remote);
		return (SessionMap) o;
	}

	@Override
	public void setMaxInactiveInterval(String sessionId, int interval) {
		Object object = CacheUtils.getCache(SESSION_STORE_REGION_KEY).get(sessionId, Level.Remote);
        ((SessionMap) object).setMaxInactiveInterval(interval);
        CacheUtils.getCache(SESSION_STORE_REGION_KEY).put(sessionId, object, interval, Level.Remote);
    }

	@Override
	public void destroy(String sessionId) {
		CacheUtils.getCache(SESSION_STORE_REGION_KEY).remove(sessionId, Level.Remote);
	}

	@Override
	public void put(String key, Object value, int timeout) {

		CacheUtils.getCache(SESSION_STORE_REGION_KEY).put(key, value, timeout, Level.Remote);
	}

	@Override
	public List<String> getKeys(String pattern) {
		List<Object> keys = CacheUtils.getCache(SESSION_STORE_REGION_KEY).keys(pattern, Level.Remote);
		List<String> kkeys = new ArrayList<String>();
		if(keys != null){
			for(Object key:keys){
				kkeys.add((String)key);
			}
		}
		return kkeys;
	}

	@Override
	public Object getValue(String key) {
		return CacheUtils.getCache(SESSION_STORE_REGION_KEY).get(key, Level.Remote);
	}

	@Override
	public void setAdd(String skey, String value) {
		CacheUtils.setAdd(skey, value);
	}

	@Override
	public Set<String> setGetAll(String skey) {
		return CacheUtils.setGet(skey);
	}
	

	@Override
	public void del(String skey) {
		CacheUtils.del(skey);
	}



	@Override
	public void listAdd(String lkey, String value) {
		CacheUtils.listAdd(lkey, value);
	}



	@Override
	public List<String> listGetAll(String lkey) {
		return CacheUtils.listGet(lkey);
	}



	@Override
	public void listRemove(String lkey, String value) {
		CacheUtils.listRemove(lkey, 0, value);
		
	}



}
