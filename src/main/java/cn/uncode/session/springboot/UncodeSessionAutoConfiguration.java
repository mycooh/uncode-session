package cn.uncode.session.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import cn.uncode.session.SessionSharingFilter;
import cn.uncode.session.data.SessionCacheManager;

/**
 * Created juny on 2017/2/28 14:11
 */
@Configuration
@EnableConfigurationProperties({UncodeSessionProperties.class})
public class UncodeSessionAutoConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UncodeSessionAutoConfiguration.class);
	
	@Autowired
	private UncodeSessionProperties uncodeSessionProperties;
	
	
	@Bean(name = "sessionCacheManager")
	public SessionCacheManager sessionCacheManager(){
		SessionCacheManager sessionCacheManager = new SessionCacheManager();
		LOGGER.info("===Uncode-Session===UncodeSessionAutoConfiguration===>SessionCacheManager inited....");
		return sessionCacheManager;
	}
	
	@Bean
    public FilterRegistrationBean<SessionSharingFilter> sessionSharingFilter() {
        FilterRegistrationBean<SessionSharingFilter> registration = new FilterRegistrationBean<SessionSharingFilter>();
        SessionSharingFilter filter = new SessionSharingFilter();
        registration.setFilter(filter);
        registration.addUrlPatterns("/*");
        registration.setName("session-filter");
        registration.setOrder(1);
        Map<String, String> initParameters = new HashMap<>();
        initParameters.put(SessionSharingFilter.TOKEN_NAME_KEY, uncodeSessionProperties.getTokenName());
        registration.setInitParameters(initParameters);
        LOGGER.info("===Uncode-Session===UncodeSessionAutoConfiguration===>SessionSharingFilter inited...");
        return registration;
    }


}
