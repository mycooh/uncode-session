package cn.uncode.session;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import cn.uncode.session.data.SessionCache;
import cn.uncode.session.data.SessionMap;

public class HttpSessionWrapper implements HttpSession {
	
	private final ServletContext servletContext;
	private SessionMap sessionMap;
	private SessionCache sessionCache;
	private boolean old;
	private String userIdentityInfo;
	
	

	public HttpSessionWrapper(SessionMap sessionMap, SessionCache sessionCache, ServletContext servletContext, String userIdentityInfo) {
		this.sessionMap = sessionMap;
		this.sessionCache = sessionCache;
		this.servletContext = servletContext;
		this.userIdentityInfo = userIdentityInfo;
	}
    @Override
	public void setMaxInactiveInterval(int interval) {
		sessionMap.setMaxInactiveInterval(interval);
		sessionCache.setMaxInactiveInterval(sessionMap.getId(), interval);
	}
	@Override
	public void setAttribute(String name, Object value) {
		sessionMap.setAttribute(name, value);
		sessionCache.put(sessionMap.getId(), sessionMap, sessionMap.getMaxInactiveInterval());
	}
    @Override
	public void removeAttribute(String name) {
		sessionMap.removeAttribute(name);
		sessionCache.put(sessionMap.getId(), sessionMap, sessionMap.getMaxInactiveInterval());
	}
    @Override
	public void putValue(String name, Object value) {
		setAttribute(name, value);
	}
    @Override
	public void removeValue(String name) {
		removeAttribute(name);
	}
    @Override
	public long getCreationTime() {
		return sessionMap.getCreationTime();
	}
    @Override
	public String getId() {
		return sessionMap.getId();
	}
    @Override
	public long getLastAccessedTime() {
		return sessionMap.getLastAccessedTime();
	}
    @Override
	public ServletContext getServletContext() {
		return servletContext;
	}

    @Override
	public int getMaxInactiveInterval() {
		return sessionMap.getMaxInactiveInterval();
	}

    @Override
	public Object getAttribute(String name) {
		return sessionMap.getAttribute(name);
	}
    @Override
	public Object getValue(String name) {
		return getAttribute(name);
	}
    @Override
	public Enumeration<String> getAttributeNames() {
		return Collections.enumeration(sessionMap.getAttributeNames());
	}
    @Override
	public String[] getValueNames() {
		Set<String> attrs = sessionMap.getAttributeNames();
		return attrs.toArray(new String[0]);
	}
    @Override
	public void invalidate() {
		sessionMap.setInvalidated(true);
		Set<String> sids = sessionCache.setGetAll(userIdentityInfo);
		for(String id:sids){
			sessionCache.destroy(id);
		}
		sessionCache.destroy(sessionMap.getId());
		sessionCache.del(userIdentityInfo);
		setCurrentSession(null);
	}

	public void setNew(boolean isNew) {
		this.old = !isNew;
	}

	@Override
	public boolean isNew() {
		return !old;
	}

	public boolean isInvalidated() {
		SessionMap sMap = sessionCache.get(sessionMap.getId());
		if(sMap == null){
			return true;
		}
		return sMap.isInvalidated();
	}

	@Override
	public HttpSessionContext getSessionContext() {
		return null;
	}

	private void setCurrentSession(HttpSessionWrapper currentSession) {
		if(currentSession == null) {
			removeAttribute(SessionHttpServletRequestWrapper.CURRENT_SESSION_ATTR);
		} else {
			setAttribute(SessionHttpServletRequestWrapper.CURRENT_SESSION_ATTR, currentSession);
		}
	}
	

}
