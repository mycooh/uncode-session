package cn.uncode.session.springboot;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "uncode.session", ignoreInvalidFields = true)
public class UncodeSessionProperties {
	
	private String tokenName;
	
	public String getTokenName() {
		return tokenName;
	}

	public void setTokenName(String tokenName) {
		this.tokenName = tokenName;
	}
	
	

}
