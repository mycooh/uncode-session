package cn.uncode.session.data;

import java.util.List;
import java.util.Set;

/**
 * 
 * @author yeweijun
 */
public interface SessionCache {
	
	/**
	 * 存储session到分布式缓存
	 * @param sessionId 当前会话id
	 * @param sessionMap 值对象
	 * @param timeout 过期时间
	 */
	public void put(String sessionId, SessionMap sessionMap, int sessionTimeOut );
	
	
	/**
	 * 从分布式缓存获取会话
	 * @param sessionId 当前会话id
	 * @return 会话对象
	 */
	public SessionMap get(String sessionId);
	
	
	/**
	 * 设置会话有效时间
	 * @param sessionId 当前会话id
	 * @param interval 有效时间，单位秒
	 */
	public void setMaxInactiveInterval(String sessionId, int interval);
	
	
	/**
	 * 销毁当前会话
	 * @param sessionId 当前会话id
	 */
	public void destroy(String sessionId);
	
	/**
	 * 保存信息
	 * <pre>
	 * 不建议组件外部使用
	 * </pre>
	 * @param key
	 * @param value
	 * @param timeout
	 */
	public void put(String key, Object value, int timeout);
	
	/**
	 * 获取列表信息
	 * <pre>
	 * 不建议组件外部使用
	 * </pre>
	 * @param pattern
	 * @return
	 */
	public List<String> getKeys(String pattern);
	/**
	 * 根据key拿到值
	 * <pre>
	 * 不建议组件外部使用
	 * </pre>
	 * @param key
	 * @return
	 * @author Jason.Li
	 * @date 2016年4月25日上午11:20:32
	 */
	public Object getValue(String key);
	
	/**
	 * 向set中添加一个值
	 * <pre>
	 * 不建议组件外部使用
	 * </pre>
	 * @param skey
	 * @param value
	 */
	public void setAdd(String skey, String value);
	
	/**
	 * 获取所有set的值
	 * <pre>
	 * 不建议组件外部使用
	 * </pre>
	 * @param skey
	 * @return
	 */
	public Set<String> setGetAll(String skey);
	

	/**
	 * 删除set,list的所有的值
	 * <pre>
	 * 不建议组件外部使用
	 * </pre>
	 * @param skey
	 * @return
	 */
	public void del(String skey);
	
	
	public void listAdd(String lkey, String value);
	
	public List<String> listGetAll(String lkey);
	
	public void listRemove(String lkey, String value);
}
